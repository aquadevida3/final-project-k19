<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page.homee');
});

Route::get('/homee', 'HomeController@index');

//ROUTE MIDDLEWARE
Route::group(['middleware'=>['auth']], function (){

    // Route Biodata
    Route::resource('biodata','BiodataController')->only([
        'index'
    ]);
    //crud customer
    Route::resource('customer','CustomerController');

    // crud product
    Route::resource('product', 'ProductController');

    // CRUD CATEGORY
    Route::resource('category','CategoryController');

    //ROUTE KOMENTAR
    Route::resource('transaction','TransactionController')->only([
        'index'
    
]);



});

Auth::routes();


