<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 'biodata';

    protected $fillable = ['name','no_hp','users_id'];

    public function user(){
        return $this->belongsTo('App\User');//belongs to ke model bernama User
    }//klo mau nampilin relasi one to one.. bisa lewat file index.blade.php, home.blade.php atau di taro di side bar
}
