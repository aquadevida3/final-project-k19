<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['name_product','price','desc','stock','category_id'];

    public function transaction(){
        return $this->hasMany('App\Transaction');
    }
}
