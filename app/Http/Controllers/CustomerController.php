<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Customer;
use File;
use RealRashid\SweetAlert\Facades\Alert;

class CustomerController extends Controller
{
    //ini untuk perintah awal di jalankan agar sebelum login ga bisa akses tombol tambah edit delete di path /customer
    public function __construct()
    {
        $this->middleware('auth')->except(['index']);

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::paginate(3);//klo mau munculin semua Film::All();
        return view ('customer.index',compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = DB::table('customer')->get();   
        return view ('customer.create', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'address'=> 'required',
            'photo'=> 'required|image|mimes:jpeg,png,jpg'
        ]);

        $namaphoto = time().'.'.$request->photo->extension();
          $request->photo->move(public_path('photo_customer'),$namaphoto);
          //buat manggil model Customer
          $customer = new Customer;
          $customer->name =$request->name;
          $customer->email =$request->email;
          $customer->address =$request->address;
          $customer->photo =$namaphoto;
          $customer->save();
          
          Alert::success('Success', 'CUSTOMER ADDED');
          return redirect('/customer');
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customer.show',compact('customer'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        return view('customer.edit',compact('customer'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'address'=> 'required',
            'photo'=> '|image|mimes:jpeg,png,jpg'
        ]);
        
        if($request->has('photo')){
            $customer = Customer::find($id);     

            $path = 'photo_customer/';                  //ini untuk delete file foto yang lama biar ga numpuk
            File::delete($path . $customer->photo);//sampe sini


            $namaphoto = time().'.'.$request->photo->extension();
            $request->photo->move(public_path('photo_customer'),$namaphoto);

            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->address = $request->address;
            $customer->photo = $namaphoto;

            $customer->save();
            Alert::success('Success!!', 'DATA UPDATED');
            return redirect('/customer');
            

        }else{
            $customer = Customer::find($id); 
            $customer->name = $request->name;
            $customer->email = $request->email;
            $customer->address = $request->address;
            
            
            $customer->save();
            Alert::success('Success!!', 'DATA UPDATED');
            return redirect('/customer');
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);     

            $path = 'photo_customer/';                  //ini untuk delete file foto yang lama biar ga numpuk
            File::delete($path . $customer->photo);//sampe sini

            $customer->delete();
            Alert::success('Success!!', 'DATA DELETED');
            return redirect('/customer');
            
    }
}
