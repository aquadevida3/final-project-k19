@extends('layout.admin_pluto')

@section('judul')
HALAMAN INDEX   
@endsection

@section('content')

<form action="/product" method="POST">
            @csrf
            <div class="form-group">
                <label>Product Name</label>
                <input type="text" class="form-control" name="name_product" placeholder="Masukkan Nama Product">
                @error('name_product')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input type="text" class="form-control" name="price" placeholder="Masukkan harga Product">
                @error('price')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea name="desc" class="form-control" cols="30" rows="5" placeholder="Masukkan Deskripsi Product"></textarea>
                @error('desc')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Stok</label>
                <input type="number" class="form-control" name="stock" placeholder="Masukkan Stock Barang">
                @error('stock')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Kategori</label><br>
                <select name="category_id" id="" class='form-control'>
                    <option value="">--Pilih Kategori--</option>
                    @foreach ($category as $item)
                        <option value="{{$item->id}}">{{$item->nama_category}}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>


@endsection
