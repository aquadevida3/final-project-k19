@extends('layout.admin_pluto')

@section('judul')
Edit Product ({{$product->name_product}})
@endsection

@section('content')

<form action="/product/{{$product->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Product</label>
                <input type="text" class="form-control" value="{{$product->name_product}}" name="name_product" placeholder="Masukkan Nama Product">
                @error('name_product')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input type="text" class="form-control" value="{{$product->price}}" name="price" placeholder="Masukkan harga Product">
                @error('price')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea name="desc" class="form-control" cols="30" rows="5" placeholder="Masukkan Deskripsi Product">{{$product->desc}}</textarea>
                @error('desc')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Stok</label>
                <input type="number" class="form-control" value="{{$product->stock}}" name="stock" placeholder="Masukkan Stock Barang">
                @error('stock')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Kategori</label><br>
                <select name="category_id" id="" class='form-control'>
                    <option value="">--Pilih Kategori--</option>
                    @foreach ($category as $item)
                        @if ($item->id === $product->category_id)
                            <option value="{{$item->id}}" selected>{{$item->nama_category}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->nama_category}}</option>
                        @endif
                    @endforeach
                </select>
                @error('category_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update Product</button>
        </form>


@endsection
