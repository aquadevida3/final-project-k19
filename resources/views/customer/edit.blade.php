@extends('layout.admin_pluto')

@section('judul')
EDIT DATA CUSTOMER {{$customer->name}}{{--INI BUAT JUDUL --}}
@endsection


@section('content')

<form action="/customer/{{$customer->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Customer Name</label>
        <input type="text" value="{{$customer->name}}" class="form-control" name="name">
      
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" value="{{$customer->email}}" class="form-control" name="email">
    
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Address</label>
        <input type="text" value="{{$customer->address}}" class="form-control" name="address">
      
      @error('address')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    
    <div class="form-group">
    <label>Photo</label>
    
    <input type="file" class="form-control" name="photo">
            
      {{-- @error('photo')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror --}}
    </div>

    <button type="submit" class="btn btn-primary">Update</button> <a href="/customer" class="btn btn-primary">Back</a>

</form>

@endsection