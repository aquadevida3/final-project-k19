<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- basic -->
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- mobile metas -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="viewport" content="initial-scale=1, maximum-scale=1">
      <!-- site metas -->
      <title>Toko Bangunan RARA</title>
      <meta name="keywords" content="">
      <meta name="description" content="">
      <meta name="author" content="">
      <!-- site icon -->
      <link rel="icon" href="{{asset('admin/images/fevicon.png')}}" type="image/png" />
      <!-- bootstrap css -->
      <link rel="stylesheet" href="{{asset('admin/css/bootstrap.min.css')}}" />
      <!-- site css -->
      <link rel="stylesheet" href="{{asset('admin/style.css')}}" />
      <!-- responsive css -->
      <link rel="stylesheet" href="{{asset('admin/css/responsive.css')}}" />
      <!-- color css -->
      <link rel="stylesheet" href="{{asset('admin/css/colors.css')}}" />
      <!-- select bootstrap -->
      <link rel="stylesheet" href="{{asset('admin/css/bootstrap-select.css')}}" />
      <!-- scrollbar css -->
      <link rel="stylesheet" href="{{asset('admin/css/perfect-scrollbar.css')}}" />
      <!-- custom css -->
      <link rel="stylesheet" href="{{asset('admin/css/custom.css')}}" />
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
	  @stack('style')
   </head>
   <body class="dashboard dashboard_1">
      <div class="full_container">
         <div class="inner_container">
            @include('sweetalert::alert')
			<!-- Sidebar  -->
            @include('partial.sidebar')
            <!-- end sidebar -->
			
			
            <!-- right content -->

            <div id="content">
               <!-- topbar -->
               <div class="topbar">
                  <nav class="navbar navbar-expand-lg navbar-light">
                     <div class="full">
                        <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i class="fa fa-bars"></i></button>
                        <div class="logo_section">  
                           <a href="index.html"><img class="img-responsive" src="{{asset('admin/images/logo/logo.png')}}" alt="#" /></a>
                        </div>
                        <div class="right_topbar">
                           <div class="icon_info">
                              
                              <ul class="user_profile_dd">
                                 <li>
                                    <a><img src="{{asset('admin/images/layout_img/user_img.jpg')}}" alt="#" /><span>Kelompok 19</span></a>
                                    <div>
                                       {{-- <a class="dropdown-item" href="/biodata/index">My Profile</a>
                                       <a class="dropdown-item" href="#"><span>Log Out</span> <i class="fa fa-sign-out"></i></a> --}}
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </nav>
               </div>
               <!-- end topbar -->
			   
               <!-- dashboard inner -->
               <section class="content">
               <div class="midde_cont">
                  <div class="container-fluid">
                     <div class="row column_title">
                        <div class="col-md-12">
                           <div class="page_title">
                              <h2>@yield('judul')</h2>                               
                           </div>
                        </section>
               <section>
                           <div class="card-body">
                              {{-- disini listing isi program --}}
                               @yield('content')
                           </div>
                        </div>
                        
                     </div>
                  </div>   
               </div>
               </section>
					 
					
                     <!-- graph -->
                     
                        <!-- end progress bar -->
                    
                  {{-- <!-- footer -->
                  <div class="container-fluid">
                     <div class="footer">
                        <p>Copyright � 2018 Designed by html.design. All rights reserved.<br><br>
                           Distributed By: <a href="https://themewagon.com/">ThemeWagon</a>
                        </p>
                     </div>
                  </div> --}}
               
               <!-- end dashboard inner -->
            </div>
         </div>
      </div>
  <!-- footer -->
                  <div class="container-fluid">
                     <div class="footer">
                        <p>Copyright � 2018 Designed by html.design. All rights reserved.<br><br>
                           Distributed By: <a href="https://themewagon.com/">ThemeWagon</a>
                        </p>
                     </div>
                  </div>
  {{-- end footer --}}

      <!-- jQuery -->
      <script src="{{asset('admin/js/jquery.min.js')}}"></script>
      <script src="{{asset('admin/js/popper.min.js')}}"></script>
      <script src="{{asset('admin/js/bootstrap.min.js')}}"></script>
      <!-- wow animation -->
      <script src="{{asset('admin/js/animate.js')}}"></script>
      <!-- select country -->
      <script src="{{asset('admin/js/bootstrap-select.js')}}"></script>
      <!-- owl carousel -->
      <script src="{{asset('admin/js/owl.carousel.js')}}"></script> 
      <!-- chart js -->
      <script src="{{asset('admin/js/Chart.min.js')}}"></script>
      <script src="{{asset('admin/js/Chart.bundle.min.js')}}"></script>
      <script src="{{asset('admin/js/utils.js')}}"></script>
      <script src="{{asset('admin/js/analyser.js')}}"></script>
      <!-- nice scrollbar -->
      <script src="{{asset('admin/js/perfect-scrollbar.min.js')}}"></script>
      <script>
         var ps = new PerfectScrollbar('#sidebar');
      </script>
      <!-- custom js -->
      <script src="{{asset('admin/js/custom.js')}}"></script>
      <script src="{{asset('admin/js/chart_custom_style1.js')}}"></script>
	  @stack('script')
   </body>
</html>