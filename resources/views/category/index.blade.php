@extends('layout.admin_pluto')

@section('judul')
Halaman List Kategori
@endsection

@section('content')

<a href="/category/create" class="btn btn-primary my-2">Tambah</a>

<div class="row">
    @forelse ($category as $item)
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                <h5> {{$item->nama_category}}</h5>
                <form action="/category/{{$item->id}}" method="POST">
                    @csrf 
                    @method('DELETE')
                    <a href="/category/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/category/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" onclick="return confirm('Are You Sure?')" class="btn btn-danger btn-sm my-2" value="Delete">
                </form>
                </div>
            </div>
        </div>
    @empty
        <h4>Kategori Belum ada</h4>
    @endforelse
</div>

  @endsection