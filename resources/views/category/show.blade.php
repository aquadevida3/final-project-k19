@extends('layout.admin_pluto')

@section('judul')
Halaman Detail Kategori {{$category->name}}
@endsection

@section('content')

<h1>{{$category->name}}</h1>

<a href="/category" class="btn btn-secondary">Kembali</a>

@endsection