@extends('layout.admin_pluto')

@section('judul')
Edit Kategori({{$category->category}})
@endsection

@section('content')

<form action="/category/{{$category->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Nama Kategori</label>
                <input type="text" class="form-control" value="{{$category->nama_category}}" name="nama_category" placeholder="Masukkan Nama Kategori">
                @error('nama_category')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update Kategory</button>  <a href="/category" class="btn btn-primary">Cancel</a>
        </form>


@endsection
